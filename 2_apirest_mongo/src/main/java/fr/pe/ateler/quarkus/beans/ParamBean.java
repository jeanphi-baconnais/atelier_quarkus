package fr.pe.ateler.quarkus.beans;

import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.PanacheMongoEntity;

@MongoEntity(collection="params")
public class ParamBean extends PanacheMongoEntity {

    private String key;

    private String value;

    public ParamBean(){

    }

    public ParamBean(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }


}
