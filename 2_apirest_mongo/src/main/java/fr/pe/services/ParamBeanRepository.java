package fr.pe.services;

import com.mongodb.MongoClient;
import fr.pe.ateler.quarkus.beans.ParamBean;
import io.quarkus.mongodb.panache.PanacheMongoRepository;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;

@ApplicationScoped
public class ParamBeanRepository implements PanacheMongoRepository<ParamBean> {

    MongoClient mongoClient;

    /**b
     * Get All params.
     * @return list of params.
     */
    public List<ParamBean> getAllParams() {
        return ParamBean.listAll();
    }

    /**
     * Save param
     * @param newParam : param to save.
     */
    public void saveParamBean(ParamBean newParam) {
        newParam.persist();
    }

    /**
     * Search by name.
     * @param keyToSearch : name to search
     * @return the first param with this name
     */
    public ParamBean searchByKey(String keyToSearch) {
        return ParamBean.find("key", keyToSearch).firstResult();
    }
}
