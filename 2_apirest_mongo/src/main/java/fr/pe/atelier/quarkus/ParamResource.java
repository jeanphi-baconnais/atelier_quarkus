package fr.pe.atelier.quarkus;


import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import fr.pe.ateler.quarkus.beans.ParamBean;
import fr.pe.services.ParamBeanRepository;

import java.util.List;


@Path("/params")
public class ParamResource {

    @Inject
    ParamBeanRepository repository;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<ParamBean> getAll() {
        return repository.getAllParams();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addParam(ParamBean newParam) {
        repository.saveParamBean(newParam);
        return Response.status(Response.Status.CREATED).build();
    }


    @GET
    @Path("/search/{key}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response searchByKey(@PathParam("key") String keyToSearch) {
        ParamBean param = repository.searchByKey(keyToSearch);
        return Response.ok().entity(param).build();
    }

}
