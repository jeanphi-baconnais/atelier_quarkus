![Quarkus](https://blog.ineat-group.com/wp-content/uploads/2019/07/quarkus_logo_vertical_default.png "") 

# Atelier Quarkus 
 

 ### 📝 Objectifs
 
 Cet atelier a pour objectif de : 
 
 - présenter Quarkus et ses avantages
 
 - permettre aux participants de manipuler Quarkus
 
 ### 💻 Pré requis
 
 - Avoir un poste linux ou mac
 
 - Avoir des connaissances Java / Jakarta (new JEE) et Docker
 
 - Avoir une version récente de docker
 
### 🗓 Temps de l'atelier : 2h

### 🖼 Le support

Le support d'introduction à l'atelier est disponible [ici](https://docs.google.com/presentation/d/1ZDJkab40TpYsqcjH-RvSvtGrMgD_DeJ4S1uwcN-Dpw0/edit?usp=sharing)

### ⚙ Configuration

- Quarkus : 1.2.1.Final
- Java 8
- GraalVM
 
 ### 📝 Exercices
 
[Exercice 0 : Api thorntail](#-exercice-0-api-thorntail) 
 
[Exercice 1 : Api Rest](#-exercice-1-api-rest) 

[Exercice 2 : Api rest / json / mongodb](#-exercice-2-api-rest-json-mongodb-panache) 

[Exercice 3 : Envoi de mail](#-exercice-3-envoi-de-mail) 

[Exercice 4 : Kafka](#-exercice-4-kafka) 

[Exercice 5 : Scheduler](#-exercice-5-scheduler)

[Exercice 6 : Data Cache](#-exercice-6-datacache) 

[Exercice 7 : GraphQL](#-exercice-7-graphql)

[Exercice 8 : Kubernetes](#-exercice-8-kubernetes)


 #### 🛠 🖋 Exercice 0 : Api Thorntail
 
Ce premier projet permet de visualiser et de comparer les temps d'exécution et de consommation mémoire entre une API Rest générée avec thorntail et une seconde API développée avec Quarkus.
 
Lancement de l'API thorntail via la commande ```java -jar -Xmx20m apirest/target/demo-thorntail.jar``` et vor ce que ça donne.

Exécuter l'API via ```java -jar -Xmx30m apirest/target/demo-thorntail.jar``` et se connecter à l'API ```http://localhost:8080/hello```
  
Exécution de l'API Quarkus via la JVM ```java -Xmx5m -jar apirestquarkus/target/apirestquarkus-1.0-SNAPSHOT-runner.jar ```
et se connecter à l'API ```http://localhost:8081/supervision```.
  
Vérification de la consommation mémoire via ```ps -o pid,rss,command | grep java | awk '{$2=int($2/1024)"M";}{ print;}'``` 

Compiler avec GraalVM (penser à vérifier que votre GRAALVM_HOME est bien *setté* 
```export GRAALVM_HOME=/Library/Java/JavaVirtualMachines/graalvm-ce-19.1.1/Contents/Home```) avec la commande ```mvn package -Pnative```. Cette commande va prendre du temps car les classes sont compilées en byte code. C'est du temps de gagné lors de l'exécution du jar -côté supersonic- et cela réduit considérablement la consommation mémoire -côté subatomic-. 

Ce profile native permet de générer un fichier exécutable ```./target/apirestquarkus-1.0-SNAPSHOT-runner```.

Vérification de la consommation mémoire 😮. Ils annonçaient un framework Subatomic ... et bien voila ! 

 #### 🛠 🖋 Exercice 1 : Api Rest
 
 Création du projet : 
 ```
 mvn io.quarkus:quarkus-maven-plugin:<version_quarkus>:create \
     -DprojectGroupId=fr.pe.atelier.quarkus \
     -DprojectArtifactId=apiRest \
     -DclassName="fr.pe.atelier.quarkus.SupervisionResource" \
     -Dpath="/supervision"
 ```
 
Exécution en mode dév : ``` mvn compile quarkus:dev ```
 
et l'application est disponible à cette url : ```http://localhost:8080/supervision```

Pour packager l'application :  ``` mvn package ```
 
 Cela produit 2 jars, qu'il est possible d'exécuter  ```java -jar target/xxxx-runner.jar ```
 
 Il est possible de packager l'application en mode native via la commande ```mvn package -Pnative -Dnative-image.docker-build=true```
 
 
 #### Compilation native vs jvm
 
 La compilation native se base sur GraalVM. C'est pour cela qu'il faut avoir une variable GRAALVM_HOME de définie avec 
 l'utilitaire **native-image** d'installé. Pour cela :
 
 ```export GRAAL_HOME=[repertoire install graalvm]```
 Se positionner dans le répertoire bin et exécuter la commande ```./gu install native-image``` (Attention des droits de 
 super utilisateur peu)
 
 Après l'exécution de la commande ```mvn package -Pnative```, un fichier **-runner** est produit dans le répertoire 
 **target**. Ce fichier est exécutable ```./xxxxx-runner```
 
Le temps d'exécution du packaging est plus important lié à la compilation de certains éléments dont tout ce qui est déclaré statique.
 
 #### Conteneurisation
 
 Pour exécuter l'exercice dans une image docker, deux options : avec ou sans la compilation native : 
 
 - Avec compilation native :
 ```
 docker build -f src/main/docker/Dockerfile.native -t atelierquarkus/kafka-native .
 docker run -i --rm -p 8080:8080 atelierquarkus/kafka-native
 ```
 
 - Sans compilation native :
 ```
 docker build -f src/main/docker/Dockerfile.jvm -t atelierquarkus/kafka-jvm .
 docker run -i --rm -p 8080:8080 atelierquarkus/kafka-jvm
 ```

 La consommation mémoire est ainsi limitée, limitant le cout généré par une mise en place sur le cloud.

 ![Conso Quarkus](https://quarkus.io/assets/images/quarkus_graphics_v3_bootmem_wide_03.png "")
 

 #### 🛠🖋 Exercice 2 : Api rest / json / mongodb panache
 
 Création du projet : 
 ```
 mvn io.quarkus:quarkus-maven-plugin:<version_quarkus>:create \
     -DprojectGroupId=fr.pe.atelier.quarkus \
     -DprojectArtifactId=apirestjsonmongo \
     -DclassName="fr.pe.atelier.quarkus.SupervisionResource" \
     -Dpath="/supervision"
 ```

Création d'un conteneur docker : ```docker run -ti --rm -p 27017:27017 mongo:4.0```

##### Extensions 
 
Les extensions possibles sont disponibles avec cette commande :  ```mvn quarkus:list-extensions```

Pour ajouter une extension : ```mvn quarkus:add-extension -Dextensions="groupId:artifactId"```, par exemple : ```mvn quarkus:add-extension -Dextensions="io.quarkus:quarkus-jdbc-postgresql"```

- Ajout de l'extension mongodb-panache : ```mvn quarkus:add-extension -Dextensions="io.quarkus:quarkus-mongodb-panache""```

Il est préférable d'utiliser les extensions quarkus que des dépendances maven 'classiques' afin de profiter du hotreload et du temps de gain de démarrage de l'extension.

##### Création d'un bean et de ressource récupérant les informations en BDD

- ParamBean.java
- ParamService.java

##### Configuration de la base de données

Pour la gestion de données dans mongo, l'extension quarkus-mongodb est possible, mais l'extension mongodb-panache simplifie le développement. 
Après avoir renseigné dans le fichier application.properties ces deux properties 
```
quarkus.mongodb.connection-string = mongodb://localhost:27017
quarkus.mongodb.database = admin
```
La connexion à la base de données est faite. Le lien entre une classe Java et une collection se fait simplement par l'extension de la classe **PanacheMongoEntity**. Cet héritage permet d'offrir un panel d'opération telles que **listAll**, **find**, **count**, **persist**, etc. 

#### Ressources disponibles 

Plusieurs ressources sont disponibles :
- la création de Param : POST http://localhost:8080/params avec un body de ce type 
```
{
    "key": "cle2",
    "value": "test 2"
}
```

- la récupération de tous les params : GET http://localhost:8080/params

- la récupération du premier param ayant la *key* spécifiée dans l'url : GET http://localhost:8080/params/search/{key}

Pour plus d'exemple, se référer à cette page : https://quarkus.io/guides/mongodb-panache


#### 🛠🖋 Exercice 3 : Envoi de mail 

Création du projet : 
```mvn io.quarkus:quarkus-maven-plugin:<version_quarkus>:create \
     -DprojectGroupId=fr.pe.atelier.quarkus \
    -DprojectArtifactId=apisendingmail \
     -DclassName="fr.pe.atelier.quarkus.SupervisionResource" \
    -Dextensions="mailer"
```

##### Création d'une ressource Mail

Création de la ressource Mail en utilisation la classe Mailer (synchrone).

##### Configuration des paramètres de mail.

Variables à renseigner dans le fichier application.properties.

🎯 Lors de l'exécution de l'application en mode dev, l'envoi de mail est mocké. Afin d'envoyer réellement un mail en dev, 
il faut ajouter cette propriété : ```quarkus.mailer.mock=false```

### 🛠🖋 Exercice 4 : Kafka

Création de l'infrastructure Kafka en exécutant ```docker-compose up &```

```
mvn io.quarkus:quarkus-maven-plugin:<version_quarkus>:create \
    -DprojectGroupId=fr.pe.atelier.quarkus \
    -DprojectArtifactId=kafka \
    -Dextensions="kafka"
```

##### Création d'un producer kafka
Création de la classe Scheduler qui va inscrire toutes les secondes un chiffre aléatoire dans une file kafka 
```generated-int```.

##### Création d'une ressource abonnée à la file kafka
Création de la classe SchedulerResource qui met à disposition une ressource REST abonnée à la même file Kafka.

##### Configuration de la file kafka dans le application.properties
```
mp.messaging.outgoing.generated-int.connector=smallrye-kafka
mp.messaging.outgoing.generated-int.topic=prices
mp.messaging.outgoing.generated-int.value.serializer=org.apache.kafka.common.serialization.StringSerializer

mp.messaging.outgoing.generated-int.value.deserializer=org.apache.kafka.common.serialization.StringDeserializer
```

##### Exécution d'une infra kafka
Création de l'infrastructure kafka en exécutant ```docker-compose up &```

##### Création d'une page HTML pour lister les résultats
Création d'une page index.html permettant de suivre l'évolutions des données présentes dans la file kafka.
Cette page est accessible à cette page : http://localhost:8080/index.html

### 🛠🖋 Exercice 5 : Scheduler

Le scheduler permet de mettre en place des taches qui s'exécutent à une fréquence régulière.

Création du projet : 
```
mvn io.quarkus:quarkus-maven-plugin:<version_quarkus>:create \
    -DprojectGroupId=fr.pe.atelier.quarkus \
    -DprojectArtifactId=5_scheduler \
    -DclassName="fr.pe.atelier.quarkus.SupervisionResource" \
    -Dextensions="scheduler"
```

Pour cela il suffit de créer un point d'entrée avec l'annotation @Scheduled :

```
    @Scheduled(every="10s")
    void testScheduler() {
        System.out.println("Scheduler ...");
    }
```

et c'est tout ! 

A la place de valeur ```every="10s"``` une tache Cron peut être positionnée : ```@Scheduled(cron="0 15 10 * * ?")``` et bien 
sur, on peut mettre la valeur de la fréquence dans le fichier properties.


### 🛠🖋 Exercice 6 : Data Cache

La gestion cache est très simple à implémenter. L'objectif est de pouvoir créer un array de type clé / valeur où la clé est un paramètre 
(ou un couple de paramètre) et la valeur est le résultat de la méthode.

Création du projet : 
```
mvn io.quarkus:quarkus-maven-plugin:<version_quarkus>:create \
    -DprojectGroupId=fr.pe.atelier.quarkus \
    -DprojectArtifactId=6_datacache \
    -DclassName="fr.pe.atelier.quarkus.SupervisionResource" \
    -Dextensions="cache,resteasy-jsonb"
```

Pour cacher une donnée, cela simplement par la création d'une méthode dans un service stateless de ce type :

```
@CacheResult(cacheName = "cache")
public String getDataCache(String key) {
    try {
        Thread.sleep(2000L);
    } catch (InterruptedException e) {
        Thread.currentThread().interrupt();
    }
    return key;
}
```

Lors de l'appel à une ressource, le premier appel avec un paramètre _key_ mettra 2 secondes à répondre. Le même appel se fera en un rien de temps.


### 🛠🖋 Exercice 7 : GraphQL

Avec l'arrivée de la version 1.5.0.Final de Quarkus, l'intégration de GralhQL est désormais possible ! 🥳


Création du projet : 
```
mvn io.quarkus:quarkus-maven-plugin:<version_quarkus>:create \
    -DprojectGroupId=fr.pe.atelier.quarkus \
    -DprojectArtifactId=7_graphql \
    -DclassName="fr.pe.atelier.quarkus.SupervisionResource" \
    -Dextensions="resteasy-jsonb,smallrye-graphql"
```

Pour commencer nous allons définir une structure de classe pour effectuer nos tests : une classe Parameter avec plusieurs attributs :
- id : int
- key : String 
- value : String 
- type : String
- active : boolean

Pour éviter d'avoir une base de données installée en local, nous utiliserons des données bouchonnées. Ces données seront stockées dans 
une classe **ParameterService**.
Ce service nous permettra d'initialiser une liste de paramètres au niveau du constructeur et fournira des méthodes permettant de :
- lister tous les paramètres
- lister tous les paramètres actifs
- récupérer un paramètre en fonction de sa *key* unique

Ensuite, créons une ressource GraphQL avec l'annotation **@GraphQLApi** :

```
    @Query("allParameters")
    @Description("Get all parameters")
    public List<ParameterBean> getAllParameters() {
        return parameterService.getAll();
    }
```

Après le redémarrage de votre projet, le schema GraphQL est accessible via ```curl http://localhost:8080/graphql/schema.graphql``` 
et dans notre exemple donne ceci :

```
curl http://localhost:8080/graphql/schema.graphql
type ParameterBean {
  active: Boolean!
  key: String
  type: String
  value: String
}

"Query root"
type Query {
  "Get all parameters"
  allParameters: [ParameterBean]
}
```

Avec GraphQL, la librairie **graphiql** est souvent utilisée car elle permet d'offrir une interface graphique facilitant les requetes GraphQL.
La dépendance GraphQL de Quarkus l'intègre de base et est donc disponible à cette url ```http://localhost:8080/graphql-ui/```.
Cela permet de faire notre première requête GraphQL pour récupérer tous les paramètres mais uniquement les champs key et value :

```
query {
  allParameters {
    key, 
    value
  }
}
```

Pour récupérer un paramètre en fonction d'une valeur de clé, la query GraphQL serait la suivante :

```
query {
  getParamByKey(key: "param1") {
    value,
    active
  }
}
```

Pour récupérer le paramètre key dans votre classe Java, il suffit de passer par l'annotation *Name* : ```public ParameterBean getParamByKey(@Name("key") String key) { ``` .

En ce qui concerne les modifications, ou mutation dans le jargon GraphQL, l'annotation **@Mutation** permet d'effectuer des mises à jour d'élément.

Bref c'est une nouvelle fois super simple de mettre en place cela avec Quarkus. 


 #### 🛠 🖋 Exercice 8 : Kubernetes

Pré requis pour cet exercice : avoir minikube (pour les postes linux ou anciens mac) ou un Docker Desktop (pour les mac OS).

Quarkus est un framework cloud native et l'intégration avec Kubernetes est très facilitée par l'extension **kubernetes**. Pour créer le projet :

```
mvn io.quarkus:quarkus-maven-plugin:<version_quarkus>:create \
    -DprojectGroupId=fr.pe.atelier.quarkus \
    -DprojectArtifactId=8_kubernetes \
    -DclassName="fr.pe.atelier.quarkus.SupervisionResource" \
    -Dextensions="kubernetes, container-image-docker"
```

Lors du packaging de l'application avec un ```mvn package```, des fichiers de configuration kubernetes sont produits dans le répertoire **target** :
- kubernetes.json
- kubernetes.yaml

J'ai pris l'option de sélectionner docker pour déployer mon image. Jib peut être également choisir via l'extension ```container-image-jib``` mais aussi **S2I**. Se référer à cette page pour plus d'informations : https://quarkus.io/guides/container-image.

Après avoir pusher mon image : ```docker build -t jeanphi/atelier-quarkus:1.0-SNAPSHOT -f src/main/docker/Dockerfile.jvm .```, je peux appliquer le fichier kubernetes.yaml avec cette commande ```kubectl apply -f target/kubernetes/kubernetes.yml```. Les services seront ainsi configurés et disponibles.

** Le service **
```
NAME              TYPE           CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
atelier-quarkus   LoadBalancer   10.102.43.230   localhost     8080:30571/TCP   30m
```

** Le pod **
```
NAME                              READY   STATUS    RESTARTS   AGE
atelier-quarkus-979d4b54d-7dv82   1/1     Running   0          31m
```

** Le déploiement **
```
NAME              READY   UP-TO-DATE   AVAILABLE   AGE
atelier-quarkus   1/1     1            1           31m
```

Bien sur, il existe plein de propriétés qui peuvent être modifiées, comme le nom de l'image générée, des annotations kubernetes, des variables d'environnements, le nombre de replicas, etc.
Plus d'infos sur ce guide : https://quarkus.io/guides/kubernetes

Pour pouvoir tester l'application, un service LoadBalancer remplace le 'ClusterIP' initialement renseigné. Cela permet tout simplement de pouvoir faire un curl sur notre application ```http://localhost:62447/hello``` et voir notre réponse. Rien de plus !

NB : pour déployer sur Kubernetes, les metadata doivent être renommés car elles sont initiées avec le nom du projet et dans cet atelier, le nom du projet n'est pas adapté.  

Dans le code de ce repo, j'ai renseigné un nombre de replicat set à 3. Cette propriété est bien générée dans le kubernetes.yaml et nous permet donc d'avoir nos 3 pods de démarré lors de l'application de notre fichier kubernetes.





