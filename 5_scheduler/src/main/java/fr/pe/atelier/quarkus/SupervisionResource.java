package fr.pe.atelier.quarkus;

import io.quarkus.scheduler.Scheduled;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/hello")
public class SupervisionResource {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        return "hello";
    }

    @Scheduled(every="10s")
    void testScheduler() {
        System.out.println("Scheduler ...");
    }
}