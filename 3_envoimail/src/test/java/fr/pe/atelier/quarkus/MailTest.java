package fr.pe.atelier.quarkus;
import io.quarkus.mailer.Mail;
import io.quarkus.mailer.MockMailbox;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.io.IOException;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@QuarkusTest
class MailTest {

    private static final String TO = "jeanphilippe.baconnais@gmail.com";

    @Inject
    MockMailbox mailbox;

    @BeforeEach
    void init() {
        mailbox.clear();
    }

    @Test
    void testTextMail() throws IOException {
        given()
                .when()
                .get("/mail")
                .then()
                .statusCode(202);
                //

        // verify that it was sent
        List<Mail> sent = mailbox.getMessagesSentTo(TO);


    }

}