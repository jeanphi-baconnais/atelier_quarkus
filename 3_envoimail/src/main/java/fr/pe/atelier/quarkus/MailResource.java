package fr.pe.atelier.quarkus;

import io.quarkus.mailer.Mail;
import io.quarkus.mailer.Mailer;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/mail")
public class MailResource {
    @Inject
    Mailer mailer;

    @GET
    public Response sendMailTest() {
        mailer.send(Mail.withHtml("jeanphilippe.baconnais@gmail.com", "A simple email from quarkus", "<p>This is my body.<p>"));
        return Response.accepted().build();
    }

}
