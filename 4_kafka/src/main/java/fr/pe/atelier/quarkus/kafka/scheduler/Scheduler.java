package fr.pe.atelier.quarkus.kafka.scheduler;


import io.reactivex.Flowable;
import io.smallrye.reactive.messaging.annotations.Broadcast;
import org.eclipse.microprofile.reactive.messaging.Outgoing;

import javax.enterprise.context.ApplicationScoped;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@ApplicationScoped
public class Scheduler {

    private Random random = new Random();

    @Outgoing("generated-int")
    @Broadcast
    public Flowable<String> generate() {

        return Flowable.interval(1, TimeUnit.SECONDS)
            .map(tick -> {
                System.out.println("scheduler 1");
                return String.valueOf(random.nextInt(100));
            });
    }
}
