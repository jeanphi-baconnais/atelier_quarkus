package fr.pe.atelier.quarkus.kafka.scheduler;


import io.smallrye.reactive.messaging.annotations.Stream;
import org.reactivestreams.Publisher;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/scheduler")
public class SchedulerResource {

    @Inject
    @Stream("generated-int") Publisher<String> infos;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String supervision() {
        return "OK";
    }

    @GET
    @Path("/stream")
    @Produces(MediaType.SERVER_SENT_EVENTS)
    public Publisher<String> stream() {
        return infos;
    }

}
