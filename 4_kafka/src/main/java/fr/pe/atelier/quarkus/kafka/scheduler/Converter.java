package fr.pe.atelier.quarkus.kafka.scheduler;

import io.smallrye.reactive.messaging.annotations.Broadcast;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Outgoing;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class Converter {
    private static final double CONVERSION_RATE = 0.88;


    @Incoming("generated-int")
    @Outgoing("new-queue")
    @Broadcast

    public double process(int priceInUsd) {
        System.out.println("converter");
        return priceInUsd * CONVERSION_RATE;
    }
}
