package fr.pe.atelier.quarkus;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/hello")
public class SupervisionResource {

    @Inject
    private DataService service;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        return "hello";
    }

    @GET
    @Path("/cache")
    @Produces(MediaType.APPLICATION_JSON)
    public String testdatacache(@QueryParam("key") String key) {
        long start = System.currentTimeMillis();
        String data = service.getDataCache(key);
        long end = System.currentTimeMillis();
        return data + " - " + (end - start);
    }

}