package fr.pe.atelier.quarkus;

import io.quarkus.test.junit.NativeImageTest;

@NativeImageTest
public class NativeSupervisionResourceIT extends SupervisionResourceTest {

    // Execute the same tests but in native mode.
}