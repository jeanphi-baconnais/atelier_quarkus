package fr.pe.atelier.quarkus;

import io.quarkus.test.junit.SubstrateTest;

@SubstrateTest
public class NativeSupervisionResourceIT extends SupervisionResourceTest {

    // Execute the same tests but in native mode.
}