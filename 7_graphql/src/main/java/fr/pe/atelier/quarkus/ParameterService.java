package fr.pe.atelier.quarkus;

import javax.enterprise.context.ApplicationScoped;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@ApplicationScoped
public class ParameterService {

    private List<ParameterBean> params;

    public ParameterService() {
        params = new ArrayList<>();
        params.add(new ParameterBean(0, "param1", "Premier paramètre", "REF", true));
        params.add(new ParameterBean(1, "param2", "Second paramètre", "REF", true));
        params.add(new ParameterBean(2, "param3", "Ancien paramètre - inactif", "REF", false));
        params.add(new ParameterBean(3, "param4", "Premier paramètre DATA", "DATA", true));
        params.add(new ParameterBean(4, "param5", "Second  paramètre DATA", "DATA", true));
    }

    public List<ParameterBean> getAll() {
        return this.params;
    }

    public List<ParameterBean> getAllActive() {
        return this.params.stream()
                .filter(param -> param.isActive())
                .collect(Collectors.toList());
    }

    public Optional<ParameterBean> getByKey(String key) {
        return this.params.stream()
                .filter(param -> key.equals(param.getKey()))
                .findFirst();
    }

    public ParameterBean deleteParam(int id) {
        return this.params.remove(id);
    }
}
