package fr.pe.atelier.quarkus;

import org.eclipse.microprofile.graphql.*;

import javax.inject.Inject;
import java.util.List;

@GraphQLApi
public class GraphQLResource {

    @Inject
    private ParameterService parameterService;

    @Query("getAllParameters")
    @Description("Get all parameters")
    public List<ParameterBean> getAllParameters() {
        return parameterService.getAll();
    }

    @Query("getParamByKey")
    @Description("Get one parameter by key")
    public ParameterBean getParamByKey(@Name("key") String key) {
        return parameterService.getByKey(key).orElse(null);
    }

    @Mutation("removeParam")
    public ParameterBean removeParam(@Name("id") int id) {
        return parameterService.deleteParam(id);
    }
}
