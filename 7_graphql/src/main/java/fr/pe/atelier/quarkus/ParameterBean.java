package fr.pe.atelier.quarkus;

public class ParameterBean {

    private int id;
    private String key;
    private String value;
    private boolean active;
    private String type;

    public ParameterBean(int id, String key, String value, String type, boolean active) {
        this.id = id;
        this.key = key;
        this.value = value;
        this.type = type;
        this.active = active;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
